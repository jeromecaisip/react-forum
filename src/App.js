import React, { Component } from 'react'
import { Provider } from 'react-redux'
import store from './store'
import ListPage from './Pages/ListPage'
import DetailPage from './Pages/DetailPage'
import CreatePostPage from './Pages/CreatePage'
import 'semantic-ui-css/semantic.min.css'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

/**
 * TODO
 * Add loading indicator while fetching
 * Improve Layout
 * Integrate formik with semantic-ui forms
 * */
class App extends Component {
  render () {
    return (
      <Provider store={store}>
        <Router>
          <Switch>
            <Route exact path='/' component={ListPage} />
            <Route exact path='/posts/new' component={CreatePostPage} />
            <Route exact path='/posts/:id' component={DetailPage} />
          </Switch>
        </Router>
      </Provider>
    )
  }
}

export default App
