import React from 'react'
import { Field, Form, Formik } from 'formik'
import { Button } from 'semantic-ui-react'
import * as Yup from 'yup'
import { getLargestId, withPosts } from '../Utils'
import { Redirect } from 'react-router'
import { withRouter } from 'react-router-dom'

const PostSchema = Yup.object().shape({
  title: Yup.string().min(2, 'Too short').required('Required'),
  description: Yup.string().min(2, 'Too short').required('Required')
})

class PostForm extends React.Component {
  componentDidMount () {
    this.props.fetchPosts()
  }

  render () {
    return (
      <Formik
        onSubmit={(values, actions) => {
          const newId = getLargestId([...this.props.posts]) + 1
          this.props.createPost(values.title, values.description, newId, this.props.history)
          // this.setState({redirectToNewPage: true})
        }}
        initialValues={{ 'title': '', description: '' }}
        validationSchema={PostSchema}
        validateOnChange={false}
        render={
          (props) => (
            <Form>
              <div>
                Title: <Field type='text' placeholder='Title' name='title' />
                {props.errors.title && <p>{props.errors.title}</p>}
              </div>
              <div>
                Description: <Field type='text' placeholder='description' name='description' />
                {props.errors.title && <p>{props.errors.description}</p>}
              </div>

              <Button type='submit'>Submit</Button>
            </Form>)
        }
      />
    )
  }
}

export default withRouter(withPosts(PostForm))
