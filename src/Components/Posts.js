import React from 'react'
import { List } from 'semantic-ui-react'
import { Link } from 'react-router-dom'

class PostList extends React.Component {
  render () {
    return (
      <List>
        {this.props.posts.map(post => <List.Item key={post.id}>
          <List.Content>
            <List.Header>
              <Link to={`/posts/${post.id}`}>{post.title}</Link>
            </List.Header>
          </List.Content>
        </List.Item>)}
      </List>
    )
  }
}

export default PostList
