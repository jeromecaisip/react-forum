import React from 'react'
import { Link } from 'react-router-dom'

export default class BackToList extends React.Component {
  render () {
    return (
      <Link to='/'>{'<< Back to list'}</Link>
    )
  }
}
