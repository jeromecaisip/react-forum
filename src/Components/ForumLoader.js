import React from 'react'
import { Dimmer, Loader } from 'semantic-ui-react'

export default class ForumLoader extends React.Component {
  render () {
    return (
      <Dimmer active inverted>
        <Loader/>
      </Dimmer>
    )
  }
}
