import React from 'react'
import { Header } from 'semantic-ui-react'

export default class Post extends React.Component {
  render () {
    return (
      <div>
        <Header>{this.props.post.title}</Header>
        <p>{this.props.post.body}</p>
      </div>
    )
  }
}
