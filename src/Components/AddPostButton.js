import React from 'react'
import { Button } from 'semantic-ui-react'
import { Link } from 'react-router-dom'

export default class AddPostButton extends React.Component {
  render () {
    return (
      <Link to='/posts/new'><Button primary>Create Post</Button></Link>
    )
  }
}
