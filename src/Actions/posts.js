export const LIST_POSTS = 'LIST_POSTS'

export const REQUEST_POSTS = 'REQUEST_POSTS'
export const REQUEST_POST = 'REQUEST_POST'
export const REQUEST_COMMENTS = 'REQUEST_COMMENTS'

export const SHOW_POST = 'SHOW_POST'
export const LIST_COMMENTS = 'LIST_COMMENTS'

export const REQUEST_CREATE_POST = 'CREATE_POST'
export const APPEND_POSTS = 'APPEND_POSTS'

export function createPost (title, body, id, history) {
  return {
    type: REQUEST_CREATE_POST,
    title,
    body,
    id,
    history
  }
}

export function fetchPosts () {
  return {
    type: REQUEST_POSTS
  }
}

export function appendPost (data) {
  return {
    type: APPEND_POSTS,
    data
  }
}

export function fetchPost (id) {
  return {
    type: REQUEST_POST,
    id
  }
}

export function showPost (post) {
  return {
    type: SHOW_POST,
    post
  }
}

export function listPosts (posts) {
  return {
    type: LIST_POSTS,
    posts
  }
}

export function fetchComments (post_id) {
  return {
    type: REQUEST_COMMENTS,
    post_id
  }
}

export function listComments (comments) {
  return {
    type: LIST_COMMENTS,
    comments
  }
}
