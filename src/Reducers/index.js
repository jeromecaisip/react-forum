import { combineReducers } from 'redux'
import { comments, post, posts } from './posts'

export default combineReducers({ posts, post, comments })
