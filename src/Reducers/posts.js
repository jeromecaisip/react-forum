import {
  LIST_COMMENTS,
  LIST_POSTS,
  REQUEST_COMMENTS,
  REQUEST_POST,
  REQUEST_POSTS,
  SHOW_POST,
  REQUEST_CREATE_POST,
  APPEND_POSTS
} from '../Actions/posts'

export function post (state = { is_fetching: false, post: {} }, action) {
  switch (action.type) {
    case REQUEST_POST:
      return {
        ...state,
        is_fetching: true
      }
    case SHOW_POST:
      return {
        ...state,
        is_fetching: false,
        post: action.post
      }
    case REQUEST_CREATE_POST:
      return {
        ...state,
        is_fetching: true
      }
    default:
      return state
  }
}

export function posts (state = { is_fetching: false, items: [], created: [] }, action) {
  switch (action.type) {
    case REQUEST_POSTS:
      return {
        ...state,
        is_fetching: true
      }
    case LIST_POSTS:
      return {
        ...state,
        is_fetching: false,
        items: [...action.posts]
      }
    case APPEND_POSTS:
      return {
        ...state,
        is_fetching: false,
        created: [...state.created, action.data]
      }
    default:
      return state
  }
}

export function comments (state = { is_fetching: false, comments: [] }, action) {
  switch (action.type) {
    case REQUEST_COMMENTS:
      return {
        ...state,
        is_fetching: true,
        comments:[],
      }
    case LIST_COMMENTS:
      return {
        ...state,
        is_fetching: false,
        comments: action.comments
      }
    default:
      return state
  }
}
