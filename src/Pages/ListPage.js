import React from 'react'
import PostList from '../Components/Posts'
import { Pagination } from 'semantic-ui-react'
import { withPosts } from '../Utils'
import AddPostButton from '../Components/AddPostButton'
import ForumLoader from '../Components/ForumLoader'
import queryString from 'query-string'

const withPagination = ({page_size}) => {
  return function (Component) {
    return function (props) {
      return <Component page_size={page_size} {...props}/>
    }
  }
}

class ListPage extends React.Component {
  //TODO Add query params for pagination
  state = {
    page: 1,
  }

  handlePaginationChange = (event, {activePage}) => {
    this.props.history.push(`?page=${activePage}`)
    this.setState((state) => {
      return {
        page: activePage,
      }
    })
  }

  componentWillMount () {
    const page_number = queryString.parse(this.props.location.search).page
    if (page_number > 1) {
      this.setState({page: page_number})
    } else {
      this.props.history.push(`/`)
    }
  }

  componentDidMount () {
    this.props.fetchPosts()
  };

  render () {

    if (this.props.is_fetching) {
      return (
        <ForumLoader/>
      )
    }
    return (
      <div>
        <AddPostButton/>
        <PostList
          limit={this.props.page_size}
          posts={[...this.props.posts].slice((this.state.page - 1) * this.props.page_size, this.state.page * this.props.page_size - 1)}
        />
        <Pagination
          siblingRange={0}
          firstItem={false}
          lastItem={false}
          totalPages={Math.floor(this.props.posts.length / this.props.page_size) + Math.ceil((this.props.posts.length % this.props.page_size) / this.props.page_size)}
          onPageChange={this.handlePaginationChange}
          defaultActivePage={this.state.page}/>
      </div>
    )
  }
}

export default withPosts(withPagination({page_size: 10})(ListPage))

