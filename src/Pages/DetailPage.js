import React, { Fragment } from 'react'
import { withComments, withPost } from '../Utils'
import Post from '../Components/Post'
import { compose } from 'redux'
import Comment from '../Components/Comment'
import BackToList from '../Components/BackToListLink'
import ForumLoader from '../Components/ForumLoader'
import {Segment,Dimmer} from 'semantic-ui-react'

class DetailPage extends React.Component {
  componentDidMount () {
    this.props.fetchPost(this.props.match.params.id)
    this.props.fetchComments(this.props.match.params.id)
  }

  render () {
    return (
      <Fragment>
        <BackToList />
        {this.props.is_fetching?<Segment><ForumLoader/></Segment>: <Post post={this.props.post}/>}
        {this.props.comments.length?this.props.comments.map(comment => <Comment key={comment.id} comment={comment} />):<Segment><Dimmer active><ForumLoader/></Dimmer></Segment>}
      </Fragment>
    )
  }
}

const withCommentAndPost = compose(withComments, withPost)
export default withCommentAndPost(DetailPage)
