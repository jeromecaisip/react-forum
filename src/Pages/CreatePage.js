import React from 'react'
import CreateForm from '../Components/NewPostForm'
import BackToList from '../Components/BackToListLink'
import { withPost } from '../Utils'

const PostCreateForm = withPost(CreateForm)
export default class CreatePage extends React.Component {
  render () {
    return (
      <div>
        <BackToList />
        <PostCreateForm />
      </div>
    )
  }
}
