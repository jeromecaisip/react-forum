import { call, put } from 'redux-saga/effects'
import { listComments, listPosts, showPost, appendPost } from '../Actions/posts'
import store from '../store'

export function * fetchPosts (action) {
  try {
    const response = yield call(fetch, 'https://jsonplaceholder.typicode.com/posts')
    const data = yield response.json()
    yield put(listPosts(data))
  } catch (e) {
    console.log(e)
  }
}

export function * fetchPost (action) {
  try {
    const response = yield call(fetch, `https://jsonplaceholder.typicode.com/posts/${action.id}`)
    if (!response.ok || action.id > 100) {
      throw 'Invalid Request'
    }

    let data = yield response.json()
    yield put(showPost(data))
  } catch (e) {
    let data = [...store.getState().posts.items, ...store.getState().posts.created].find(function (item) {
      return item.id == action.id
    })
    yield put(showPost(data))
  }
}

export function * createPost (action) {
  try {
    const response = yield call(fetch, `https://jsonplaceholder.typicode.com/posts/`, {
      method: 'POST',
      body: { title: action.title, body: action.body, userId: 1 }
    })
    const data = yield response.json()
    data.id = action.id
    data.title = action.title
    data.body = action.body
    yield put(appendPost(data))
    action.history.push('/')
  } catch (e) {
    console.log(e)
  }
}

export function * fetchComments (action) {
  try {
    const response = yield call(fetch, `https://jsonplaceholder.typicode.com/posts/${action.post_id}/comments`)
    const data = yield response.json()
    yield put(listComments(data))
  } catch (e) {
    console.log(e)
  }
}
