import { all, takeLatest } from 'redux-saga/effects'
import { createPost, fetchComments, fetchPost, fetchPosts } from './posts'
import { REQUEST_COMMENTS, REQUEST_CREATE_POST, REQUEST_POST, REQUEST_POSTS } from '../Actions/posts'

export default function * rootSaga () {
  yield all([
    yield takeLatest(REQUEST_POSTS, fetchPosts),
    yield takeLatest(REQUEST_POST, fetchPost),
    yield takeLatest(REQUEST_COMMENTS, fetchComments),
    yield takeLatest(REQUEST_CREATE_POST, createPost)
  ])
}
