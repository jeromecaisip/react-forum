import React from 'react'
import { connect } from 'react-redux'
import { fetchComments, fetchPost, fetchPosts, createPost } from '../Actions/posts'

export function getLargestId (items) {
  let maxId = items.reduce(function (maxId, item) {
    return Math.max(maxId, item.id)
  }, 0)
  return maxId
}

const mapPostsStateToProps = (state) => {
  return {
    posts: [...state.posts.items, ...state.posts.created]
  }
}

const mapPostsDispatchToProps = {
  fetchPosts
}

export const withPosts = connect(mapPostsStateToProps, mapPostsDispatchToProps)

const mapPostStateToProps = (state) => {
  console.log(state.post)
  return {
    ...state.post,
    post: state.post.post
  }
}

const mapPostDispatchToProps = {
  fetchPost,
  createPost
}

export const withPost = connect(mapPostStateToProps, mapPostDispatchToProps)

const mapCommentsStateToProps = (state) => {
  return {
    ...state.comments,
    post: state.comments.comments
  }
}

const mapCommenttDispatchToProps = {
  fetchComments
}

export const withComments = connect(mapCommentsStateToProps, mapCommenttDispatchToProps)
